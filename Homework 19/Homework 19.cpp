// Homework 19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>


using namespace std;

class Animal
{
protected:
	string voice;
public:
	Animal() {};
	Animal(string _voice) :voice(_voice) {
		_voice = "�����";
	}
	virtual void getVoice() {
		cout << voice << endl;
	};
		
	

private:

};

class Dog: public Animal
{
protected:
	string voice;

public:
	Dog() {};
	Dog(string _voice):voice(_voice) {
		_voice = "���";
	}
	void getVoice() {
		cout << voice << endl;
	};
		
	

private:

};

class Cat: public Animal {
public:
	string voice;

public:
	Cat() {};
	Cat(string _voice) {
		voice = _voice;
	}
	
	void getVoice() {
		cout << voice << endl;
	};



};

class Bird:public Animal {
protected:
	string voice;

public:
	Bird() {};
	Bird(string _voice):voice(_voice) {
		_voice = "��������";
	}
	
	void getVoice(){
		cout << voice << endl;
	};


};

class Fox : public Animal {
protected:
	string voice;
public:
	Fox() {};
	Fox(string _voice) :voice(_voice) {
		_voice = "AHEEHIHAHIHOOO";
	};
	void getVoice() {
		cout << voice << endl;
	};
	
};
void GetVoice(string voice) {
	cout << voice;
}

int main()
{
	const int size = 4;
	setlocale(LC_ALL, "ru");
	Cat cat("���");
	Dog dog("���");
	Bird bird("�����");
	Fox fox("AHEEHIHAHOO");
	Animal* _animal[size]{ &cat, &dog, &bird, &fox };
	
	_animal[1]->getVoice();
	for (int i = 0; i < size; i++) {
		_animal[i]->getVoice();
	};
		
	

}
	

